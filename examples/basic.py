# 使用Paradox对y=Ax+b进行梯度计算，并输出x的梯度。 整个过程由图计算和自动求导完成
import paradox as pd

A = pd.Constant([[1, 2], [1, 3]], name='A')
x = pd.Variable([0, 0], name='x')
b = pd.Constant([3, 4], name='b')

# 按照求和的方式对矩阵降维
loss = pd.reduce_sum((A @ x - b) ** 2) / 2

# 损失函数及其值
e = pd.Engine(loss, x)
print('loss formula =\n{}\n'.format(loss))
print('loss =\n{}\n'.format(e.value()))

# x的梯度计算公式及其值
x_gradient = e.gradient(x)
print('x gradient formula =\n{}\n'.format(x_gradient))
print('x gradient =\n{}\n'.format(pd.Engine(x_gradient).value()))

# 求出x的简化公式及其值
s = pd.Simplification()
s.simplify(x_gradient)
print('x gradient formula =\n{}\n'.format(x_gradient))
print('x gradient =\n{}\n'.format(pd.Engine(x_gradient).value()))
